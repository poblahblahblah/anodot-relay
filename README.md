anodot-relay
============

Relay graphite metrics to anodot backend

It currently supports:

 - line protocol
 - pickle protocol

Installation
------------
    git clone https://bitbucket.org/anodotengineering/anodot-relay
    cd anodot-relay
    npm install

Usage
-----
    cd anodot-relay/bin
    ./relay config.json

config.json
-----------

      {
          "url": "https://api.anodot.com",
          "token": "<YOUR API TOKEN>",
          "log": "error",
          "showStats": false,
          "sendStats": true,
          "plainPort": 2003,
          "picklePort": 2004,
          "flushInterval": 5000,
          "metricFilter": "",
          "metricRewrite": [],
          "metricPrefix": "",
          "metricCounter":""
      }
 
 
- url: anodot service url, default https://api.anodot.com
 
- token: anodot api token 
 
- log: log level default error
  
- showStats: print to stdout performance statistics every flushInterval, default false
  
- sendStats: send statistics metrics to anodot backend, default true

- plainPort: listener port for plain text protocol, default 2003
 
- picklePort: listener port for pickle protocol, default 2004
 
- flushInterval: number in milliseconds to flush metrics, default 10,000
  
- metricFilter: javascript regex that filters out (blacklist) metrics not to be send to anodot, default ""
                
                example: "metricFilter": "token1|token2|token3"
 
- metricRewrite: rewrite rules for metrics tokens default, []
                
                example: "metricRewrite": [{"regex": "token1", "token": "what="}]
                
                will rewrite metric token1.token2.token3 to what=token1.token2.token3

- metricPrefix: add prefix to every metric send via the relay

- metricCounter: javascript regex that will set metrics to be sent as a counter to anodot, default ""

                example: "metricCounter": "target_type=counter"
                
                will send all metrics containing "target_type=counter" as counter to anodot 


- backlog: server connections backlog, default 512


- maxConnections: max number of concurrent connections opened to anodot service, default 100

- sortBeforeSend: sort by timestamp Tx buffer before send to anodot, default false

Running Tests
-------------

To run the tests, install Mocha then run:

    cd anodot-relay/test
    mocha
